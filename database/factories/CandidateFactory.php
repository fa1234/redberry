<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CandidateFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $minSalary = rand(1000, 3000);
        $maxSalary = rand($minSalary + 1000, 10000);
        $linkedin = [null, 'https://www.linkedin.com/'];

        return [
            'first_name' => $this->faker->firstName(),
            'last_name' => $this->faker->firstName(),
            'position_id' => function () {
                return \App\Models\Position::factory(1)->create()->first()->id;
            },

            'min_salary' => $minSalary,
            'max_salary' => $maxSalary,
            'linkedin' => $linkedin[array_rand($linkedin)],

        ];
    }
}
