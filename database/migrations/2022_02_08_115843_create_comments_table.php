<?php

use App\Models\Candidate;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('candidate_id');
            $table->text('text');
            $table->enum('status', [
                Candidate::STATUS_INITIAL,
                Candidate::STATUS_FIRST_CONTACT,
                Candidate::STATUS_INTERVIEW,
                Candidate::STATUS_TECH_ASSIGNMENT,
                Candidate::STATUS_REJECTED,
                Candidate::STATUS_HIRED,
            ]);
            $table->timestamps();

            $table->foreign('candidate_id')
                ->references('id')
                ->on('candidates')
                ->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
