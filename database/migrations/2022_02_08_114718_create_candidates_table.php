<?php

use App\Models\Candidate;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidates', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('position_id');
            $table->string('first_name');
            $table->string('last_name');
            $table->integer('min_salary')->nullable();
            $table->integer('max_salary')->nullable();
            $table->string('linkedin')->nullable();
            $table->string('pdf')->nullable();
            $table->enum('status', [
                Candidate::STATUS_INITIAL,
                Candidate::STATUS_FIRST_CONTACT,
                Candidate::STATUS_INTERVIEW,
                Candidate::STATUS_TECH_ASSIGNMENT,
                Candidate::STATUS_REJECTED,
                Candidate::STATUS_HIRED,
            ])->default(Candidate::STATUS_INITIAL);
            $table->timestamps();

            $table->foreign('position_id')
                ->references('id')
                ->on('positions')
                ->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidates');
    }
}
