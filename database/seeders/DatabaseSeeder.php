<?php

namespace Database\Seeders;

use App\Models\Candidate;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //create positions
        $positions =  \App\Models\Position::factory(10)->create();

        //create tags
        $tags =  \App\Models\Tag::factory(20)->create();

        //create candidates
        foreach ($positions as $position) {

            $statuses  = array_keys(Candidate::STATUSES_ARRAY);
            $status  = $statuses[array_rand($statuses)];

            $candidates = \App\Models\Candidate::factory(rand(1, 5))->create(['position_id' => $position->id, 'status' => $status]);

            foreach ($candidates as $candidate) {
                //attach tags to single candidate
                // $tag_ids = $tags->shuffle()->limit(rand(1, 5))->get()->pluck('id')->toArray();
                $tag_ids = $tags->shuffle()->slice(0, rand(1, 5))->pluck('id')->toArray();

                $candidate->tags()->sync($tag_ids);

                //create candidate status timeline based on current status

                //check that candidate's current status is not initial
                if ($candidate->status > Candidate::STATUS_INITIAL) {
                    foreach ($statuses as $key => $status) {
                        //prevent to create initial status comment
                        if ($key != 0) {
                            if ($candidate->status < $status) {
                                break;
                            } else {
                                //create old one's comment
                                \App\Models\Comment::factory(1)->create(['candidate_id' => $candidate->id, 'status' => $status]);
                            }
                        }
                    }
                }
            }
        }
    }
}
