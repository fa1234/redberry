<?php

use App\Http\Controllers\Api\CandidateController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('candidates', [CandidateController::class, 'index'])->name('candidates');
Route::get('candidates/{id}', [CandidateController::class, 'show'])->name('candidates.show');
Route::get('candidates/{id}/comments', [CandidateController::class, 'comments'])->name('candidates.comments');
Route::post('candidates/create', [CandidateController::class, 'create'])->name('candidates.create');
Route::put('candidates/{id}/update', [CandidateController::class, 'update'])->name('candidates.update');
//update candidate status
Route::put('candidates/{id}/update-status', [CandidateController::class, 'updateStatus'])->name('candidates.updateStatus');
