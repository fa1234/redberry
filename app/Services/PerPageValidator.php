<?php

namespace App\Services;

class PerPageValidator
{

    public static function validate($per_page)
    {
        $maximum_record_for_per_page = config('meta.maximum_record_for_per_page');
        $default_record_for_per_page = config('meta.default_record_for_per_page');

        if ($per_page > $maximum_record_for_per_page) {
            $per_page = $maximum_record_for_per_page;
        }

        if ($per_page < 1) {
            $per_page = $default_record_for_per_page;
        }

        return (int) $per_page;
    }
}
