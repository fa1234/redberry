<?php

namespace App\Http\Resources;

use App\Models\Candidate;
use Illuminate\Http\Resources\Json\JsonResource;

class CandidateResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $statuses = Candidate::STATUSES_ARRAY;

        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'min_salary' => $this->min_salary,
            'max_salary' => $this->max_salary,
            'pdf' => $this->pdfPath(),
            'status' => $this->status,
            'status_name' => $statuses[$this->status],
            'position' => $this->position ? new PositionResource($this->position) : null,
            'tags' => TagResource::collection($this->tags),
        ];
    }
}
