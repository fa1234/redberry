<?php

namespace App\Http\Resources;

use App\Models\Candidate;
use Illuminate\Http\Resources\Json\JsonResource;

class CommentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $statuses = Candidate::STATUSES_ARRAY;
        return [
            'id' => $this->id,
            'text' => $this->text,
            'status' => $this->status,
            'status_name' => $statuses[$this->status],
        ];
    }
}
