<?php

namespace App\Http\Controllers\Api;

use Throwable;
use App\Models\Comment;
use App\Models\Candidate;
use Illuminate\Http\Request;
use App\Services\PerPageValidator;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use App\Http\Resources\CommentResource;
use App\Http\Resources\CandidateResource;
use Illuminate\Validation\ValidationException;
use App\Http\Requests\Api\UpdateCandidateStatusRequest;
use App\Http\Requests\Api\CreateOrUpdateCandidateRequest;

class CandidateController extends Controller
{
    public function index(Request $request)
    {
        $per_page = PerPageValidator::validate($request->per_page);

        $sort = $request->sort;
        $keyword = $request->keyword;
        $status = $request->status;

        $query = Candidate::with(['position', 'tags']);

        //sort records
        if (!empty($sort)) {
            if ($sort == 'created_at') {
                $query = $query->orderBy('created_at', 'desc');
            }
            if ($sort == 'created_at-') {
                $query = $query->orderBy('created_at', 'asc');
            }

            if ($sort == 'updated_at') {
                $query = $query->orderBy('updated_at', 'desc');
            }
            if ($sort == 'updated_at-') {
                $query = $query->orderBy('updated_at', 'asc');
            }

            if ($sort == 'status') {
                $query = $query->orderBy('status', 'desc');
            }

            if ($sort == 'status-') {
                $query = $query->orderBy('status', 'asc');
            }
        }

        //filter by status
        if (!empty($status)) {
            $query = $query->where('status', $status);
        }

        //search by keyword
        if (!empty($keyword)) {

            if (!empty($keyword)) {
                $query = $query->where(function ($q) use ($keyword) {
                    $q->where('first_name', 'like', '%' . $keyword . '%')
                        ->orWhere('last_name', 'like', '%' . $keyword . '%')
                        ->orWhereHas('position', function ($q) use ($keyword) {
                            $q->where('name', 'like', '%' . $keyword . '%');
                        })->orWhereHas('tags', function ($q) use ($keyword) {
                            $q->where('name', 'like', '%' . $keyword . '%');
                        });
                });
            }
        }

        $query = $query->paginate($per_page);

        return CandidateResource::collection($query);
    }

    public function comments($id)
    {
        $candidate = Candidate::find($id);

        if (!$candidate) {
            return response()->json("candidate not found", 404);
        }

        return CommentResource::collection($candidate->comments);
    }

    public function show($id)
    {
        $candidate = Candidate::find($id);
        if (!$candidate) {
            return response()->json("candidate not found", 404);
        }

        return new CandidateResource($candidate);
    }

    public function create(CreateOrUpdateCandidateRequest $request)
    {
        DB::beginTransaction();

        try {
            $candidate = new Candidate();
            $candidate->first_name = $request->first_name;
            $candidate->last_name = $request->last_name;
            $candidate->position_id = $request->position_id;
            $candidate->min_salary = $request->min_salary;
            $candidate->max_salary = $request->max_salary;
            $candidate->linkedin = $request->linkedin;
            $candidate->status = Candidate::STATUS_INITIAL;

            //check if request has file
            if ($request->hasFile("pdf")) {
                $name = md5(date('Y-m-d_H-i-s')) . '.pdf';
                $request->pdf->storeAs('public/pdf', $name);
                $candidate->pdf = $name;
            }

            $candidate->save();

            //check if tag ids exist and attach them to candidate
            if (!empty($request->tag_ids)) {
                $candidate->tags()->sync($request->tag_ids);
            }

            DB::commit();

            return new CandidateResource($candidate);
        } catch (Throwable $e) {
            report($e);
            DB::rollback();

            return response()->json("something went wrong", 500);
        }
    }

    public function update(CreateOrUpdateCandidateRequest $request, $id)
    {
        $candidate = Candidate::find($id);

        if (!$candidate) {
            return response()->json("candidate not found", 404);
        }

        DB::beginTransaction();

        try {
            $candidate->first_name = $request->first_name;
            $candidate->last_name = $request->last_name;
            $candidate->position_id = $request->position_id;
            $candidate->min_salary = $request->min_salary;
            $candidate->max_salary = $request->max_salary;
            $candidate->linkedin = $request->linkedin;
            $candidate->status = Candidate::STATUS_INITIAL;

            //check if request has file
            if ($request->hasFile("pdf")) {
                //delete old file
                if (File::exists(storage_path('app/public/pdf/' . $candidate->pdf))) {
                    File::delete(storage_path('/app/public/pdf/' . $candidate->pdf));
                }

                $name = md5(date('Y-m-d_H-i-s')) . '.pdf';
                $request->pdf->storeAs('public/pdf', $name);
                $candidate->pdf = $name;
            }
            $candidate->save();

            //check if tag ids exist and attach them to candidate
            if (!empty($request->tag_ids)) {
                $candidate->tags()->sync($request->tag_ids);
            }

            DB::commit();

            return new CandidateResource($candidate);
        } catch (Throwable $e) {
            report($e);
            DB::rollback();

            return response()->json("something went wrong", 500);
        }
    }

    public function updateStatus(UpdateCandidateStatusRequest $request, $id)
    {
        $candidate = Candidate::find($id);
        if (!$candidate) {
            return response()->json("candidate not found", 404);
        }

        //check if candidate's existing status and request status is same or not
        if ($candidate->status == $request->status) {
            throw ValidationException::withMessages(['status' => 'candidate already has same status']);
        }

        DB::beginTransaction();

        try {
            $candidate->status = $request->status;
            $candidate->save();
            //create new comment based on status
            $comment  = new Comment;
            $comment->candidate_id = $candidate->id;
            $comment->text = $request->comment;
            $comment->status = $candidate->status;
            $comment->save();

            DB::commit();

            return new CommentResource($comment);
        } catch (Throwable $e) {
            report($e);
            DB::rollback();

            return response()->json("something went wrong", 500);
        }
    }
}
