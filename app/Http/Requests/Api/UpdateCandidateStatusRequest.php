<?php

namespace App\Http\Requests\Api;

use App\Models\Candidate;
use Illuminate\Foundation\Http\FormRequest;

class UpdateCandidateStatusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'status' => 'required|in:' . Candidate::STATUS_INITIAL . ',' .
                Candidate::STATUS_FIRST_CONTACT . ',' .
                Candidate::STATUS_INTERVIEW . ',' .
                Candidate::STATUS_TECH_ASSIGNMENT . ',' .
                Candidate::STATUS_REJECTED . ',' .
                Candidate::STATUS_HIRED,
            'comment' => 'required|min:2|max:1000',
        ];
    }
}
