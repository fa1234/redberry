<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class CreateOrUpdateCandidateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string|min:2|max:50',
            'last_name' => 'required|string|min:2|max:50',
            'position_id' => 'required|exists:positions,id',
            'min_salary' => 'nullable|integer|between:1,999998',
            'max_salary' => 'nullable|integer|between:2,999999|gt:min_salary',
            'linkedin' => 'nullable|url|max:255',
            'pdf' => 'nullable|mimes:pdf|max:5120',
            'tag_ids' => 'nullable|array',
            'tag_ids.*' => 'required|integer|exists:tags,id',
        ];
    }
}
