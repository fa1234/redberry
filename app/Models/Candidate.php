<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
    use HasFactory;

    public const STATUS_INITIAL = 1;
    public const STATUS_FIRST_CONTACT = 2;
    public const STATUS_INTERVIEW = 3;
    public const STATUS_TECH_ASSIGNMENT = 4;
    public const STATUS_REJECTED = 5;
    public const STATUS_HIRED = 6;

    public const STATUSES_ARRAY = [
        self::STATUS_INITIAL => 'Initial',
        self::STATUS_FIRST_CONTACT => 'First Contact',
        self::STATUS_INTERVIEW => 'Interview',
        self::STATUS_TECH_ASSIGNMENT => 'Tech Assignment',
        self::STATUS_REJECTED => 'Rejected',
        self::STATUS_HIRED => 'Hired',
    ];

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'candidate_tags', 'candidate_id', 'tag_id');
    }

    public function position()
    {
        return $this->belongsTo(Position::class, 'position_id');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class, 'candidate_id');
    }

    public function pdfPath()
    {
        return $this->pdf ? asset('/pdf/' . $this->pdf) : null;
    }
}
