<?php

namespace Tests;


trait TestingData
{

    public function candidates($params = [], $count = 1)
    {
        return  \App\Models\Candidate::factory($count)->create($params);
    }

    public function positions($params = [], $count = 1)
    {
        return  \App\Models\Position::factory($count)->create($params);
    }

    public function comments($params = [], $count = 1)
    {
        return  \App\Models\Comment::factory($count)->create($params);
    }

    public function tags($params = [], $count = 1)
    {
        return  \App\Models\Tag::factory($count)->create($params);
    }
}
