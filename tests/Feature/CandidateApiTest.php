<?php

namespace Tests\Feature;

use Tests\TestCase;
use Tests\TestingData;
use App\Models\Candidate;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CandidateApiTest extends TestCase
{
    use RefreshDatabase, TestingData;
    /**
     * A basic feature test example.
     *
     * @return void
     */

    /** @test */
    public function check_candidates_endpoint()
    {
        //create candidates
        $this->candidates(['status' => Candidate::STATUS_INITIAL], 50);
        $this->candidates(['status' => Candidate::STATUS_HIRED], 5);

        $defaultPerPage = config('meta.default_record_for_per_page');

        //check default per page
        $response = $this->json('get', '/api/candidates', []);
        $response->assertStatus(200);
        $data = json_decode($response->getContent())->data;
        $this->assertEquals($defaultPerPage, count($data));

        //check if per page works
        $perPage = config('meta.maximum_record_for_per_page');

        $response = $this->json('get', '/api/candidates', ['per_page' => $perPage]);
        $response->assertStatus(200);
        $data = json_decode($response->getContent())->data;
        $this->assertEquals($perPage, count($data));

        //check if status filter works
        $response = $this->json('get', '/api/candidates', ['per_page' => 10, 'status' => Candidate::STATUS_HIRED]);
        $response->assertStatus(200);
        $data = json_decode($response->getContent())->data;
        $this->assertEquals(5, count($data));

        //check if search works
        $position = $this->positions(['name' => 'Lead Developer'], 1)->first();

        $this->candidates(['first_name' => 'first_name', 'last_name' => 'last_name', 'position_id' => $position->id], 1);

        //search by first name
        $response = $this->json('get', '/api/candidates', ['per_page' => 10, 'keyword' => 'first_name']);
        $response->assertStatus(200);
        $data = json_decode($response->getContent())->data;
        $this->assertEquals(1, count($data));

        //search by last name
        $response = $this->json('get', '/api/candidates', ['per_page' => 10, 'keyword' => 'last']);
        $response->assertStatus(200);
        $data = json_decode($response->getContent())->data;
        $this->assertEquals(1, count($data));

        //search by position
        $response = $this->json('get', '/api/candidates', ['per_page' => 10, 'keyword' => 'Developer']);
        $response->assertStatus(200);
        $data = json_decode($response->getContent())->data;
        $this->assertEquals(1, count($data));

        //search by tags
        $tag_id = $this->tags(['name' => 'Laravel'], 1)->first()->pluck('id')->toArray();
        $candidates = Candidate::limit(3)->get();
        foreach ($candidates as $candidate) {
            $candidate->tags()->sync($tag_id);
        }

        $response = $this->json('get', '/api/candidates', ['per_page' => 10, 'keyword' => 'Laravel']);
        $response->assertStatus(200);
        $data = json_decode($response->getContent())->data;
        $this->assertEquals(3, count($data));

        //check status sort
        //find out how many record has specific status
        $response = $this->json('get', '/api/candidates', ['per_page' => 5, 'sort' => 'status']);
        $data = json_decode($response->getContent())->data;

        $this->assertTrue($data[0]->status == Candidate::STATUS_HIRED);
        $this->assertTrue($data[0]->status == Candidate::STATUS_HIRED);
        $this->assertTrue($data[0]->status == Candidate::STATUS_HIRED);
    }

    /** @test */
    public function check_single_candidate_endpoint()
    {
        $this->candidates([], 1);
        $id = Candidate::first()->id;

        $response = $this->json('get', '/api/candidates/' . $id, []);
        $response->assertStatus(200);
    }

    /** @test */
    public function check_single_candidate_comments_endpoint()
    {
        $this->candidates(['status' => Candidate::STATUS_FIRST_CONTACT], 1);
        $candidate = Candidate::first();
        //create comments
        $this->comments(['candidate_id' => $candidate->id, 'status' => $candidate->status]);

        $response = $this->json('get', '/api/candidates/' . $candidate->id . '/comments', []);
        $response->assertStatus(200);
        $data = json_decode($response->getContent())->data;
        $this->assertEquals(1, count($data));
    }

    /// Create Candidate tests ///

    /** @test */
    public function check_create_candidate_required_fields_validation()
    {
        //create position
        $position_id = $this->positions([], 1)->first()->id;
        //check required field validation

        //case 1 required fields
        $response = $this->json('post', '/api/candidates/create', [
            // 'first_name' => 'first_name',
            // 'last_name' => 'last_name',
            // 'position_id' => $position_id,
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);

        $this->assertTrue(isset($data['errors']['first_name']));
        $this->assertTrue(isset($data['errors']['last_name']));
        $this->assertTrue(isset($data['errors']['position_id']));

        //case 2 first name last name min
        $response = $this->json('post', '/api/candidates/create', [
            'first_name' => 'f',
            'last_name' => 'l',
            'position_id' => $position_id,
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['errors']['first_name']));
        $this->assertTrue(isset($data['errors']['last_name']));


        //case 3 first name lst name max
        $response = $this->json('post', '/api/candidates/create', [
            'first_name' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam maxime ex obcaecati reiciendis aperiam. Dignissimos sit itaque, nesciunt autem deleniti libero maiores ex, explicabo minima ut, atque suscipit corrupti odio. Commodi et iusto, a quidem aperiam velit odit doloribus ex repudiandae, hic vero omnis numquam error quae ratione in sequi possimus?',
            'last_name' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam maxime ex obcaecati reiciendis aperiam. Dignissimos sit itaque, nesciunt autem deleniti libero maiores ex, explicabo minima ut, atque suscipit corrupti odio. Commodi et iusto, a quidem aperiam velit odit doloribus ex repudiandae, hic vero omnis numquam error quae ratione in sequi possimus?',
            'position_id' => $position_id,
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['errors']['first_name']));
        $this->assertTrue(isset($data['errors']['last_name']));

        //case 4 invalid position_id
        $response = $this->json('post', '/api/candidates/create', [
            'first_name' => 'first_name',
            'last_name' => 'last_name',
            'position_id' => 1111,
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['errors']['position_id']));

        //it should create record
        $response = $this->json('post', '/api/candidates/create', [
            'first_name' => 'first_name',
            'last_name' => 'last_name',
            'position_id' => $position_id,
        ]);

        $response->assertStatus(201);

        $this->assertDatabaseHas('candidates', [
            'first_name' => 'first_name',
            'last_name' => 'last_name',
            'position_id' => $position_id
        ]);
    }

    /** @test */
    public function check_create_candidate_non_required_fields_validation()
    {
        //create position
        $position_id = $this->positions([], 1)->first()->id;
        //create tags
        $tag_id_one = $this->tags([], 1)->first()->id;
        $tag_id_two = $this->tags([], 1)->first()->id;

        //case 1 - min max salary integer
        $response = $this->json('post', '/api/candidates/create', [
            'first_name' => 'first_name',
            'last_name' => 'last_name',
            'position_id' => $position_id,
            'min_salary' => 'a',
            'max_salary' => 'a',
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['errors']['min_salary']));
        $this->assertTrue(isset($data['errors']['max_salary']));

        //case 2 - min-max salary min value
        $response = $this->json('post', '/api/candidates/create', [
            'first_name' => 'first_name',
            'last_name' => 'last_name',
            'position_id' => $position_id,
            'min_salary' => 0,
            'max_salary' => 1,
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['errors']['min_salary']));
        $this->assertTrue(isset($data['errors']['max_salary']));

        //case 3 - min-max salary max value
        $response = $this->json('post', '/api/candidates/create', [
            'first_name' => 'first_name',
            'last_name' => 'last_name',
            'position_id' => $position_id,
            'min_salary' => 9999998,
            'max_salary' => 9999999,
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);

        $this->assertTrue(isset($data['errors']['min_salary']));
        $this->assertTrue(isset($data['errors']['max_salary']));

        //case 4 - max needs to be greater then min
        $response = $this->json('post', '/api/candidates/create', [
            'first_name' => 'first_name',
            'last_name' => 'last_name',
            'position_id' => $position_id,
            'min_salary' => 1000,
            'max_salary' => 999,
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);

        $this->assertTrue(isset($data['errors']['max_salary']));

        //case 5 - tags_id[*] needs to ne integer
        $response = $this->json('post', '/api/candidates/create', [
            'first_name' => 'first_name',
            'last_name' => 'last_name',
            'position_id' => $position_id,
            'min_salary' => 1000,
            'max_salary' => 2000,
            'tag_ids' => ['aa'],
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);

        $this->assertTrue(isset($data['errors']['tag_ids.0']));

        //case 6 - tags_id[*] needs to be existed in our table
        $response = $this->json('post', '/api/candidates/create', [
            'first_name' => 'first_name',
            'last_name' => 'last_name',
            'position_id' => $position_id,
            'min_salary' => 1000,
            'max_salary' => 2000,
            'tag_ids' => [111, 222],
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['errors']['tag_ids.0']));
        $this->assertTrue(isset($data['errors']['tag_ids.1']));

        //case 7 - linkedin url url format
        $response = $this->json('post', '/api/candidates/create', [
            'first_name' => 'first_name',
            'last_name' => 'last_name',
            'position_id' => $position_id,
            'min_salary' => 1000,
            'max_salary' => 2000,
            'tag_ids' => [$tag_id_one, $tag_id_two],
            'linkedin' => 'some string'
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);

        $this->assertTrue(isset($data['errors']['linkedin']));

        //case 8 - pdf mime type
        $response = $this->json('post', '/api/candidates/create', [
            'first_name' => 'first_name',
            'last_name' => 'last_name',
            'position_id' => $position_id,
            'min_salary' => 1000,
            'max_salary' => 2000,
            'tag_ids' => [$tag_id_one, $tag_id_two],
            'linkedin' => 'https://www.linkedin.com/',
            'pdf' => UploadedFile::fake()->image('avatar.jpg')
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);

        $this->assertTrue(isset($data['errors']['pdf']));

        //case 9 - pdf max size
        $response = $this->json('post', '/api/candidates/create', [
            'first_name' => 'first_name',
            'last_name' => 'last_name',
            'position_id' => $position_id,
            'min_salary' => 1000,
            'max_salary' => 2000,
            'tag_ids' => [$tag_id_one, $tag_id_two],
            'linkedin' => 'https://www.linkedin.com/',
            'pdf' =>  UploadedFile::fake()->create('document.pdf', 60000),
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['errors']['pdf']));





        //it should create candidate with valid record
        $response = $this->json('post', '/api/candidates/create', [
            'first_name' => 'first_name',
            'last_name' => 'last_name',
            'position_id' => $position_id,
            'min_salary' => 1000,
            'max_salary' => 2000,
            'tag_ids' => [$tag_id_one, $tag_id_two],
            'linkedin' => 'https://www.linkedin.com/',
            'pdf' =>  UploadedFile::fake()->create('document.pdf', 5000),
        ]);

        $response->assertStatus(201);
        $data = json_decode($response->getContent())->data;


        $this->assertDatabaseHas('candidates', [
            'id' => $data->id,
            'first_name' => 'first_name',
            'last_name' => 'last_name',
            'position_id' => $position_id,
            'min_salary' => 1000,
            'max_salary' => 2000,
            'linkedin' => 'https://www.linkedin.com/',
        ]);

        $this->assertDatabaseHas('candidate_tags', [
            'candidate_id' => $data->id,
            'tag_id' => $tag_id_one,
        ]);

        $this->assertDatabaseHas('candidate_tags', [
            'candidate_id' => $data->id,
            'tag_id' => $tag_id_two,
        ]);

        $file = Candidate::first()->pdf;
        //check if file exists
        Storage::disk('local')->assertExists('public/pdf/' . $file);
    }

    /// update candidate tests ///

    /** @test */
    public function check_update_candidate_required_fields_validation()
    {
        $position_id = $this->positions([], 1)->first()->id;

        $candidate = $this->candidates([], 1)->first();

        //case 1 required fields
        $response = $this->json('put', '/api/candidates/' . $candidate->id . '/update', [
            // 'first_name' => 'first_name',
            // 'last_name' => 'last_name',
            // 'position_id' => $position_id,
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);

        $this->assertTrue(isset($data['errors']['first_name']));
        $this->assertTrue(isset($data['errors']['last_name']));
        $this->assertTrue(isset($data['errors']['position_id']));

        //case 2 first name last name min
        $response = $this->json('put', '/api/candidates/' . $candidate->id . '/update', [
            'first_name' => 'f',
            'last_name' => 'l',
            'position_id' => $position_id,
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['errors']['first_name']));
        $this->assertTrue(isset($data['errors']['last_name']));


        //case 3 first name lst name max
        $response = $this->json('put', '/api/candidates/' . $candidate->id . '/update', [
            'first_name' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam maxime ex obcaecati reiciendis aperiam. Dignissimos sit itaque, nesciunt autem deleniti libero maiores ex, explicabo minima ut, atque suscipit corrupti odio. Commodi et iusto, a quidem aperiam velit odit doloribus ex repudiandae, hic vero omnis numquam error quae ratione in sequi possimus?',
            'last_name' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam maxime ex obcaecati reiciendis aperiam. Dignissimos sit itaque, nesciunt autem deleniti libero maiores ex, explicabo minima ut, atque suscipit corrupti odio. Commodi et iusto, a quidem aperiam velit odit doloribus ex repudiandae, hic vero omnis numquam error quae ratione in sequi possimus?',
            'position_id' => $position_id,
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['errors']['first_name']));
        $this->assertTrue(isset($data['errors']['last_name']));

        //case 4 invalid position_id
        $response = $this->json('put', '/api/candidates/' . $candidate->id . '/update', [
            'first_name' => 'first_name',
            'last_name' => 'last_name',
            'position_id' => 1111,
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['errors']['position_id']));

        //it should create record
        $response = $this->json('put', '/api/candidates/' . $candidate->id . '/update', [
            'first_name' => 'first_name',
            'last_name' => 'last_name',
            'position_id' => $position_id,
        ]);

        $response->assertStatus(200);

        $this->assertDatabaseHas('candidates', [
            'first_name' => 'first_name',
            'last_name' => 'last_name',
            'position_id' => $position_id
        ]);
    }

    /** @test */
    public function check_update_candidate_non_required_fields_validation()
    {
        //create position
        $position_id = $this->positions([], 1)->first()->id;
        //create candidate
        $candidate = $this->candidates([], 1)->first();
        //create tags
        $tag_id_one = $this->tags([], 1)->first()->id;
        $tag_id_two = $this->tags([], 1)->first()->id;

        //case 1 - min max salary integer
        $response = $this->json('put', '/api/candidates/' . $candidate->id . '/update', [
            'first_name' => 'first_name',
            'last_name' => 'last_name',
            'position_id' => $position_id,
            'min_salary' => 'a',
            'max_salary' => 'a',
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['errors']['min_salary']));
        $this->assertTrue(isset($data['errors']['max_salary']));

        //case 2 - min-max salary min value
        $response = $this->json('put', '/api/candidates/' . $candidate->id . '/update', [
            'first_name' => 'first_name',
            'last_name' => 'last_name',
            'position_id' => $position_id,
            'min_salary' => 0,
            'max_salary' => 1,
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['errors']['min_salary']));
        $this->assertTrue(isset($data['errors']['max_salary']));

        //case 3 - min-max salary max value
        $response = $this->json('put', '/api/candidates/' . $candidate->id . '/update', [
            'first_name' => 'first_name',
            'last_name' => 'last_name',
            'position_id' => $position_id,
            'min_salary' => 9999998,
            'max_salary' => 9999999,
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);

        $this->assertTrue(isset($data['errors']['min_salary']));
        $this->assertTrue(isset($data['errors']['max_salary']));

        //case 4 - max needs to be greater then min
        $response = $this->json('put', '/api/candidates/' . $candidate->id . '/update', [
            'first_name' => 'first_name',
            'last_name' => 'last_name',
            'position_id' => $position_id,
            'min_salary' => 1000,
            'max_salary' => 999,
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);

        $this->assertTrue(isset($data['errors']['max_salary']));

        //case 5 - tags_id[*] needs to ne integer
        $response = $this->json('put', '/api/candidates/' . $candidate->id . '/update', [
            'first_name' => 'first_name',
            'last_name' => 'last_name',
            'position_id' => $position_id,
            'min_salary' => 1000,
            'max_salary' => 2000,
            'tag_ids' => ['aa'],
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);

        $this->assertTrue(isset($data['errors']['tag_ids.0']));

        //case 6 - tags_id[*] needs to be existed in our table
        $response = $this->json('put', '/api/candidates/' . $candidate->id . '/update', [
            'first_name' => 'first_name',
            'last_name' => 'last_name',
            'position_id' => $position_id,
            'min_salary' => 1000,
            'max_salary' => 2000,
            'tag_ids' => [111, 222],
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['errors']['tag_ids.0']));
        $this->assertTrue(isset($data['errors']['tag_ids.1']));

        //case 7 - linkedin url url format
        $response = $this->json('post', '/api/candidates/create', [
            'first_name' => 'first_name',
            'last_name' => 'last_name',
            'position_id' => $position_id,
            'min_salary' => 1000,
            'max_salary' => 2000,
            'tag_ids' => [$tag_id_one, $tag_id_two],
            'linkedin' => 'some string'
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);

        $this->assertTrue(isset($data['errors']['linkedin']));

        //case 8 - pdf mime type
        $response = $this->json('post', '/api/candidates/create', [
            'first_name' => 'first_name',
            'last_name' => 'last_name',
            'position_id' => $position_id,
            'min_salary' => 1000,
            'max_salary' => 2000,
            'tag_ids' => [$tag_id_one, $tag_id_two],
            'linkedin' => 'https://www.linkedin.com/',
            'pdf' => UploadedFile::fake()->image('avatar.jpg')
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);

        $this->assertTrue(isset($data['errors']['pdf']));

        //case 9 - pdf max size
        $response = $this->json('post', '/api/candidates/create', [
            'first_name' => 'first_name',
            'last_name' => 'last_name',
            'position_id' => $position_id,
            'min_salary' => 1000,
            'max_salary' => 2000,
            'tag_ids' => [$tag_id_one, $tag_id_two],
            'linkedin' => 'https://www.linkedin.com/',
            'pdf' =>  UploadedFile::fake()->create('document.pdf', 60000),
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['errors']['pdf']));

        //it should create candidate with valid record
        $response = $this->json('put', '/api/candidates/' . $candidate->id . '/update', [
            'first_name' => 'first_name',
            'last_name' => 'last_name',
            'position_id' => $position_id,
            'min_salary' => 1000,
            'max_salary' => 2000,
            'tag_ids' => [$tag_id_one, $tag_id_two],
            'linkedin' => 'https://www.linkedin.com/',
            'pdf' =>  UploadedFile::fake()->create('document.pdf', 5000),
        ]);

        $response->assertStatus(200);
        $data = json_decode($response->getContent())->data;

        $this->assertDatabaseHas('candidates', [
            'id' => $data->id,
            'first_name' => 'first_name',
            'last_name' => 'last_name',
            'position_id' => $position_id,
            'min_salary' => 1000,
            'max_salary' => 2000,
            'linkedin' => 'https://www.linkedin.com/',
        ]);

        $this->assertDatabaseHas('candidate_tags', [
            'candidate_id' => $data->id,
            'tag_id' => $tag_id_one,
        ]);

        $this->assertDatabaseHas('candidate_tags', [
            'candidate_id' => $data->id,
            'tag_id' => $tag_id_two,
        ]);

        $file = Candidate::first()->pdf;
        //check if file exists
        Storage::disk('local')->assertExists('public/pdf/' . $file);
        sleep(1);

        //check if old file removed
        $response = $this->json('put', '/api/candidates/' . $candidate->id . '/update', [
            'first_name' => 'first_name',
            'last_name' => 'last_name',
            'position_id' => $position_id,
            'min_salary' => 1000,
            'max_salary' => 2000,
            'tag_ids' => [$tag_id_one, $tag_id_two],
            'linkedin' => 'https://www.linkedin.com/',
            'pdf' =>  UploadedFile::fake()->create('document.pdf', 5000),
        ]);

        $response->assertStatus(200);
        $newFile = Candidate::first()->pdf;
        Storage::disk('local')->assertMissing('public/pdf/' . $file);
        Storage::disk('local')->assertExists('public/pdf/' . $newFile);
    }

    /** @test */
    public function check_update_candidate_status_functionality()
    {
        $candidate = $this->candidates(['status' => Candidate::STATUS_INITIAL], 1)->first();

        //case 1 - check required fields
        $response = $this->json('put', '/api/candidates/' . $candidate->id . '/update-status', [
            // 'status' => Candidate::STATUS_FIRST_CONTACT,
            // 'comment' => 'some text',
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);

        $this->assertTrue(isset($data['errors']['status']));
        $this->assertTrue(isset($data['errors']['comment']));

        //case 2 - check status invalide value
        $response = $this->json('put', '/api/candidates/' . $candidate->id . '/update-status', [
            'status' => 111,
            'comment' => 'some text',
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['errors']['status']));

        //case 3 - check comment min
        $response = $this->json('put', '/api/candidates/' . $candidate->id . '/update-status', [
            'status' => Candidate::STATUS_FIRST_CONTACT,
            'comment' => 's',
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);

        $this->assertTrue(isset($data['errors']['comment']));

        //case 4 - check comment max
        $response = $this->json('put', '/api/candidates/' . $candidate->id . '/update-status', [
            'status' => Candidate::STATUS_FIRST_CONTACT,
            'comment' => 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quae dolore ipsa quidem totam non pariatur sapiente dolor, quisquam commodi maxime debitis nemo quia sit eligendi quasi, obcaecati excepturi atque esse aliquam magnam eum illo similique fugit. Debitis laborum, dolorum deserunt quam earum perspiciatis nobis! Accusamus, assumenda corporis officia minima id nesciunt tempora dolore aut sapiente ex? A perspiciatis repudiandae, unde minus animi sed laudantium commodi ipsa maxime voluptate labore eaque dolores in harum incidunt consequatur quam reprehenderit aut quia natus architecto distinctio aperiam culpa similique? Nostrum reiciendis ipsa nam hic eius explicabo ipsam ad adipisci, quas aperiam atque totam reprehenderit deserunt eum earum! Veniam nihil eveniet possimus pariatur similique rem provident laudantium excepturi! Ipsa eum nulla quod sequi suscipit delectus vitae aliquam, sed saepe accusamus. Nemo sapiente nobis cum deserunt explicabo maxime accusantium fuga obcaecati illum excepturi laborum exercitationem assumenda et optio incidunt expedita, aspernatur totam. Delectus iusto optio mollitia accusamus numquam voluptatum, quae exercitationem earum cumque labore unde ea facilis repellat quis minus perferendis, asperiores id possimus dolore ipsa obcaecati nobis illo inventore! Eligendi quo, quisquam illum dolorum magnam ducimus voluptatibus sunt laboriosam non molestias maiores. Quae reiciendis quos nemo est dicta porro modi sequi eaque labore accusamus fugit expedita cumque quod tenetur minima eveniet qui explicabo enim et voluptatum, nihil consectetur officiis cum vel? Voluptate fugiat molestiae hic officia debitis culpa ipsam nulla accusamus, blanditiis porro in explicabo illo tempore veritatis maiores quam at mollitia exercitationem quis eaque est vero. Nesciunt, natus! Voluptatem sunt esse neque aperiam totam non quas dignissimos ipsam animi nesciunt veritatis, exercitationem magnam ad, modi, consectetur ipsum enim aspernatur eveniet nulla. Laborum, est quaerat, repellat quam minima, illo eius facere voluptatum dolorum optio fugit ex dolor ipsam quasi sequi quis sit enim. Eaque saepe maxime pariatur maiores nostrum vitae quo fugiat dolorem sit fugit enim neque corrupti porro quis soluta ut quasi praesentium blanditiis veritatis laborum, iusto voluptates odit. Dolore deleniti qui optio quia tenetur nostrum odit! Commodi animi voluptatem eveniet nulla, earum repellendus eos quod, aliquid est at praesentium officiis quidem ipsa tempore facere eaque distinctio, minus aliquam laborum? Facere, culpa at nam atque incidunt optio, vel sequi enim vero nostrum omnis provident veniam iste cum ratione. Commodi nostrum tenetur magni autem eligendi! Ea sequi quidem quo saepe sapiente voluptate. Sint quam consectetur et beatae expedita totam natus dicta corrupti commodi laudantium possimus voluptates asperiores, obcaecati hic, maxime voluptatibus facilis sit. Nisi dolores consequuntur ipsa facilis unde voluptas consequatur vero reiciendis soluta cupiditate quisquam, maiores fugit fugiat praesentium itaque accusantium vel a alias quae nemo mollitia ut quis. Odio voluptate sint, nisi labore neque accusamus vel ea recusandae, doloremque impedit iusto sunt at sit nobis, eum aliquid similique et reprehenderit quam! Repudiandae et rerum obcaecati assumenda omnis adipisci doloremque numquam similique nam nostrum quae tenetur quod ut eveniet cumque, magni ipsa? Ipsam aspernatur recusandae, sint accusantium officia obcaecati vel neque placeat, itaque sed vitae consequuntur voluptatem ex enim odit. Assumenda iste voluptas consequuntur iusto doloremque magni. Fuga laudantium quisquam placeat commodi, esse nulla, minima autem vel maxime sapiente doloremque quidem? Quos veritatis ex accusantium, ipsam maiores cupiditate. Recusandae quas dolore praesentium, assumenda laboriosam aperiam totam voluptatibus debitis deleniti, corrupti qui sunt quo repellendus esse quod. Sequi, sapiente explicabo! Accusantium totam incidunt iure quisquam soluta. Labore perspiciatis odit fugiat nam. Nulla debitis, eos ipsum alias saepe delectus facere quo repellendus, cum consequuntur et vel distinctio! Possimus dignissimos officiis nesciunt inventore dolorum itaque voluptates obcaecati repudiandae, vel cumque tempora in earum harum voluptatibus deserunt voluptas! Enim porro impedit optio ad accusamus illo aspernatur quam pariatur incidunt quas nulla libero dignissimos, dolorum consectetur ipsa, corrupti commodi. Facilis error nemo neque dolore unde, nihil enim? Doloribus, consectetur! Sapiente tempore hic a maxime aliquam id labore quia ea, molestiae voluptatum ex velit ipsam non, saepe qui praesentium cum eveniet accusamus, repellendus doloremque inventore natus. Quia amet dicta accusantium quas odio nostrum ratione ea aut hic delectus, excepturi ducimus, distinctio rerum quisquam? Nam ab reiciendis modi quam, ullam ex fuga, architecto quo, sapiente deleniti repellat dolore beatae nemo et accusantium quasi error saepe hic. Expedita ab modi, ea veniam atque laboriosam, ipsum est quia odio quae deserunt! Numquam veniam iusto saepe repellendus voluptate, incidunt necessitatibus eos voluptatem, commodi in obcaecati quos ipsam, atque laboriosam ut sint ducimus veritatis dolorem? Similique quos incidunt excepturi, nesciunt in officiis deserunt minus cumque nemo corporis iure velit, eum quidem cum at. Soluta, asperiores? Officia, veritatis atque? Quod saepe repellendus reiciendis doloribus vel vero ullam nesciunt debitis consectetur eveniet, perspiciatis ea cumque, quisquam ex illo voluptate porro quasi cum odio facilis enim tenetur officia dolorum. Nobis voluptates inventore temporibus eaque cum recusandae laborum. Sit id asperiores deleniti delectus aspernatur veniam quaerat. Voluptatum illum repudiandae facere, assumenda similique labore blanditiis odio ipsam in natus hic quis tenetur suscipit nam magni quasi iste ullam qui libero, vitae ipsa adipisci perferendis id quia? Delectus esse accusamus laborum rerum maiores beatae, perferendis quis qui! Minus sunt quod, quae temporibus tenetur quia ea? Mollitia ratione alias accusantium fugiat atque maxime officiis modi odio ipsam sequi, error tempore minima dolorem rerum earum iure! Dolor quasi rerum ipsum obcaecati cupiditate nostrum saepe adipisci, dignissimos, molestiae aperiam officia aliquid at culpa? Sint recusandae vel deserunt, mollitia debitis adipisci dolorum. Expedita corporis non possimus qui atque quas laborum voluptate temporibus, ducimus iste dolor? Laudantium ipsum tempore, ut quasi quod praesentium pariatur a mollitia nobis laboriosam quas nemo, delectus voluptatibus neque? Sit distinctio eos blanditiis omnis vel aspernatur nemo corporis id vitae, eveniet reprehenderit voluptatibus veritatis obcaecati similique commodi quibusdam dolore exercitationem ducimus optio voluptate ipsa qui ipsam voluptates. Incidunt architecto itaque natus! Optio a vero beatae nisi velit voluptates, similique ipsum exercitationem tenetur eligendi architecto, ipsa incidunt unde eveniet expedita nam minima accusamus, quis repellat commodi alias minus illum? Repellat error fugit officiis neque, minima sapiente numquam eveniet quaerat rerum quam quod voluptatibus voluptas eligendi exercitationem eum perspiciatis adipisci. Illo et omnis suscipit adipisci laborum veniam necessitatibus consequuntur praesentium ipsum quia natus ipsam, consequatur sint a eveniet fugit temporibus repellendus quos, quam totam in!',
        ]);

        $response->assertStatus(422);
        $data = json_decode($response->getContent(), true);
        $this->assertTrue(isset($data['errors']['comment']));

        //it should update candidate status and create candidate comment record about status
        $response = $this->json('put', '/api/candidates/' . $candidate->id . '/update-status', [
            'status' => Candidate::STATUS_FIRST_CONTACT,
            'comment' => 'some text',
        ]);

        $response->assertStatus(201);

        $this->assertDatabaseHas('candidates', [
            'id' => $candidate->id,
            'status' => Candidate::STATUS_FIRST_CONTACT,
        ]);

        $this->assertDatabaseHas('comments', [
            'candidate_id' => $candidate->id,
            'status' => Candidate::STATUS_FIRST_CONTACT,
            'text' => 'some text',

        ]);
    }
}
