<?php

return [

    'maximum_record_for_per_page' => env('MAXIMUM_RECORD_FOR_PER_PAGE', 20),
    'default_record_for_per_page' => env('DEFAULT_RECORD_FOR_PER_PAGE', 10),
];
